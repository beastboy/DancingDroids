use std::str::FromStr;
use std::{
    fs::File,
    io::{prelude::*, BufReader},
};
#[derive(Debug, PartialEq)]
/// Option d'orientation
pub enum Orientation {
    Right,
    Left,
    Move,
    Vide,
}
#[derive(Debug, PartialEq)]
/// orientation au spawn
pub enum SpawnOrientation {
    North,
    South,
    Vide,
}

/// prend en parametre une lettre, renvoie une orientation selon celle-ci
pub fn orientation_func(orientation: char) -> Orientation {
    match orientation {
        'R' => Orientation::Right,
        'L' => Orientation::Left,
        'F' => Orientation::Move,
        _ => Orientation::Vide,
    }
}

// prend en parametre une lettre, renvoie une SpawnOrientation selon celle-ci
pub fn spawn_orientation_func(orientation: &str) -> SpawnOrientation {
    match orientation {
        "N" => SpawnOrientation::North,
        "S" => SpawnOrientation::South,
        _ => SpawnOrientation::Vide,
    }
}
#[derive(Debug, PartialEq)]
// Structure de robot voir si il faut modifier pour y ajouter l'orientation
pub struct Robots {
    pub id: i32,
    pub orientation: String,
    pub fleche: char,
    pub x_pos: usize,
    pub y_pos: usize,
    pub commande: Vec<char>,
}
/// Defini le sens des fleches selon le valeur de i
pub fn fleche(i: usize) -> char {
    if i == 0 {
        return '↑';
    }
    if i == 1 {
        return '→';
    }
    if i == 2 {
        return '↓';
    }
    if i == 3 {
        return '←';
    }
    return '0';
}
/// Place les robots et affiche le plateau
pub fn affichage(grid: &mut Vec<Vec<char>>, robot_vec: &Vec<Robots>, world_size: &mut Vec<usize>) {
    // println!("x : {}, y : {}",robot.x_pos,robot.y_pos);
    // place tout les robots dans la grid
    for i in 0..robot_vec.len() {
        for _j in 0..robot_vec.len() {
            if robot_vec[i].x_pos <= world_size[0] - 1 && robot_vec[i].y_pos <= world_size[1] - 1 {
                grid[robot_vec[i].x_pos][robot_vec[i].y_pos] = robot_vec[i].fleche;
            }
        }
    }
    for i in 0..world_size[1] {
        for j in 0..world_size[0] {
            print!("{} ", grid[i][j]);
        }
        println!("");
    }
    println!("===========");
}

/// Effectue les deplacement des robots, appel `affichage()` et previens des collisions s'il y'en a
pub fn deplacement(
    robots_vec: &mut Vec<Robots>,
    grid: &mut Vec<Vec<char>>,
    world_size: &mut Vec<usize>,
) {
    let mut carole: Vec<usize> = Vec::new();
    for _cmp in 0..robots_vec.len() {
        carole.push(0);
    }
    if robots_vec.len() > 0 {
        for i in 0..robots_vec[0].commande.len() {
            for n in 0..robots_vec.len() {
                if i == 0 {
                    if spawn_orientation_func(&robots_vec[n].orientation) == SpawnOrientation::South
                    {
                        carole[n] = 2;
                    } else {
                        carole[n] = 0
                    }
                }
                if orientation_func(robots_vec[n].commande[i]) == Orientation::Left {
                    if carole[n] != 0 {
                        carole[n] = carole[n] - 1;
                    } else {
                        carole[n] = 3;
                    }
                }
                if orientation_func(robots_vec[n].commande[i]) == Orientation::Right {
                    if carole[n] != 3 {
                        carole[n] = carole[n] + 1;
                    } else {
                        carole[n] = 0;
                    }
                }

                if orientation_func(robots_vec[n].commande[i]) == Orientation::Move {
                    if carole[n] == 0 {
                        if robots_vec[n].y_pos > 0 {
                            if grid[robots_vec[n].x_pos][robots_vec[n].y_pos - 1] == '.' {
                                grid[robots_vec[n].x_pos][robots_vec[n].y_pos] = '.';
                                robots_vec[n].y_pos = robots_vec[n].y_pos - 1;
                            } else {
                                println!(
                                    "collision de {} en :{} {}",
                                    robots_vec[n].id,
                                    robots_vec[n].x_pos,
                                    robots_vec[n].y_pos - 1
                                )
                            }
                        }
                    }
                    if carole[n] == 2 {
                        if robots_vec[n].y_pos < world_size[0] - 1 {
                            if grid[robots_vec[n].x_pos][robots_vec[n].y_pos + 1] == '.' {
                                grid[robots_vec[n].x_pos][robots_vec[n].y_pos] = '.';
                                robots_vec[n].y_pos = robots_vec[n].y_pos + 1;
                            } else {
                                println!(
                                    "collision de {} en :{} {}",
                                    robots_vec[n].id,
                                    robots_vec[n].x_pos,
                                    robots_vec[n].y_pos + 1
                                )
                            }
                        }
                    }
                    if carole[n] == 1 {
                        if robots_vec[n].x_pos < world_size[0] - 1 {
                            if grid[robots_vec[n].x_pos + 1][robots_vec[n].y_pos] == '.' {
                                grid[robots_vec[n].x_pos][robots_vec[n].y_pos] = '.';
                                robots_vec[n].x_pos = robots_vec[n].x_pos + 1;
                            } else {
                                println!(
                                    "collision de {} en :{} {}",
                                    robots_vec[n].id, robots_vec[n].x_pos, robots_vec[n].y_pos
                                )
                            }
                        }
                    }
                    if carole[n] == 3 {
                        if robots_vec[n].x_pos > 0 {
                            if grid[robots_vec[n].x_pos - 1][robots_vec[n].y_pos] == '.' {
                                grid[robots_vec[n].x_pos][robots_vec[n].y_pos] = '.';
                                robots_vec[n].x_pos = robots_vec[n].x_pos - 1;
                            } else {
                                println!(
                                    "collision de {} en :{} {}",
                                    robots_vec[n].id,
                                    robots_vec[n].x_pos - 1,
                                    robots_vec[n].y_pos
                                )
                            }
                        }
                    }
                }
                robots_vec[n].fleche = fleche(carole[n]);
                println!(
                    "id : {}, x : {}, y : {},instructions: {}, tour {}",
                    robots_vec[n].id,
                    robots_vec[n].x_pos,
                    robots_vec[n].y_pos,
                    robots_vec[n].commande[i],
                    i
                );
                affichage(grid, &robots_vec, world_size);
            }
        }
    }
}
/// Cet fonctions creer un nouveau robot
pub fn new_robot(
    id: i32,
    orientation: String,
    fleche: char,
    x_pos: usize,
    y_pos: usize,
    commande: Vec<char>,
) -> Robots {
    Robots {
        id: id,
        orientation: orientation,
        fleche: fleche,
        x_pos: x_pos,
        y_pos: y_pos,
        commande: commande,
    }
}
/// Recupere les instructions du fichier `.txt`
pub fn instruction_parse(filename: &str) -> Vec<String> {
    let file = File::open(filename).expect("no such file");
    let buf = BufReader::new(file);
    buf.lines()
        .map(|l| l.expect("Could not parse line"))
        .collect()
}

/// Creer des robots d'apres les informations de `instruction_parse`
/// Normalise la taille la taille des instructions
pub fn normalize() -> (Vec<usize>, Vec<Robots>) {
    let instructions = instruction_parse("two_robots.txt");
    let mut robots_vec: Vec<Robots> = Vec::new();
    let iter = instructions[0].split_whitespace();
    let mut world_size: Vec<usize> = Vec::new();
    // prend la premiere ligne et le separe en deux puise le parse en usize
    if instructions[0] != " " {
        for i in iter {
            let num: usize = match i.parse() {
                Ok(num) => num,
                Err(_e) => continue,
            };
            world_size.push(num);
        }
        if world_size.len() == 1 {
            world_size.push(world_size[0])
        }
        if world_size.len() == 0 {
            world_size.push(5);
            world_size.push(5);
        }
    }

    let blank_space = &instructions[1];
    let mut id = 0;
    if instructions[1] != " ".to_string() {
        for i in 0..instructions.len() {
            // si la ligne est vide alors on prend l'index suivant
            if &instructions[i] == blank_space && i + 1 < instructions.len() {
                // on regarde si la ligne suivante est vide
                if &instructions[i + 1] != blank_space {
                    // si elle n'est pas vide on regarde si il y'a au moins 2 caractere
                    if instructions[i + 1].chars().count() >= 2 {
                        if instructions[i + 1].split_whitespace().count() >= 3 {
                            let mut j = instructions[i + 1].split_whitespace();
                            id = id + 1;
                            let mut x_pos: usize = usize::from_str(
                                j.next().expect("cet ligne ne contient pas de x_pos"),
                            )
                            .unwrap_or_default();
                            let mut y_pos: usize = usize::from_str(
                                j.next().expect("cet ligne ne contient pas de y_pos"),
                            )
                            .unwrap_or_default();
                            let orientation = j.next().expect("pas d'orientation");
                            let commande: Vec<_> = instructions[i + 2].chars().collect();
                            let mut i: usize = 0;
                            if orientation == "N" {
                                i = i + 0;
                            } else if orientation == "S" {
                                i = i + 2;
                            }
                            if x_pos >= world_size[0] {
                                x_pos = world_size[0] - 1
                            };
                            if y_pos >= world_size[1] {
                                y_pos = world_size[0] - 1
                            };
                            robots_vec.push(new_robot(
                                id,
                                orientation.to_string(),
                                fleche(i),
                                x_pos,
                                y_pos,
                                commande,
                            ));
                            // On donne une taille unique au commande en fonctions de la plus longue
                            // Pour eviter au maximum les erreur
                            for i in 0..robots_vec.len() {
                                let mut n = 0;
                                for j in 0..robots_vec.len() {
                                    if n < robots_vec[j].commande.len() {
                                        n = robots_vec[j].commande.len();
                                    }
                                }
                                for _r in 0..n {
                                    if robots_vec[i].commande.len() < n {
                                        robots_vec[i].commande.push('o');
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    (world_size, robots_vec)
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    // Test de orientation_func test aussi l'enum Orientation par la meme ocasion
    fn _test_orientation_func() {
        assert_eq!(orientation_func('R'), Orientation::Right);
        assert_eq!(orientation_func('L'), Orientation::Left);
        assert_eq!(orientation_func('F'), Orientation::Move);
        assert_eq!(orientation_func('M'), Orientation::Vide);
        assert_eq!(orientation_func('_'), Orientation::Vide);
    }
    #[test]
    // Test de Spawnorientation_func test aussi l'enum Spawnorientation_func par la meme ocasion
    fn _test_spawn_orientation_func() {
        assert_eq!(spawn_orientation_func("S"), SpawnOrientation::South);
        assert_eq!(spawn_orientation_func("N"), SpawnOrientation::North);
        assert_eq!(spawn_orientation_func("_"), SpawnOrientation::Vide);
    }
    #[test]
    // Test de l'orientation de la fleche
    fn _test_fleche() {
        assert_eq!(fleche(0), '↑');
        assert_eq!(fleche(1), '→');
        assert_eq!(fleche(2), '↓');
        assert_eq!(fleche(3), '←');
        assert_eq!(fleche(4), '0');
        assert_eq!(fleche(40000), '0');
    }

    #[test]
    // Test de l'affichage de la premiere grid avant toutes modifications de la func deplacement
    fn _test_affichage() {
        let mut world_size: Vec<usize> = vec![5, 5];
        let mut state = vec![vec!['.'; world_size[0]]; world_size[1]];
        let mut robots_vec: Vec<Robots> = Vec::new();
        let robot_un = Robots {
            id: 1,
            orientation: "N".to_string(),
            fleche: '←',
            y_pos: 0,
            x_pos: 1,
            commande: vec!['F', 'F', 'F', 'F', 'L', 'R', 'F'],
        };
        robots_vec.push(robot_un);
        affichage(&mut state, &robots_vec, &mut world_size);
        assert_eq!(state[0][1], '.');
        deplacement(&mut robots_vec, &mut state, &mut world_size);
    }

    #[test]
    // Test de l'affichage de la derniere grid apres toutes modifications de la func deplacement
    fn _test_deplacement() {
        let mut world_size: Vec<usize> = vec![5, 5];
        let mut state = vec![vec!['.'; world_size[0]]; world_size[1]];
        let mut robots_vec: Vec<Robots> = Vec::new();
        let robot_un = Robots {
            id: 1,
            orientation: "N".to_string(),
            fleche: '←',
            y_pos: 0,
            x_pos: 1,
            commande: vec!['F', 'F', 'F', 'F', 'L', 'R', 'F'],
        };
        robots_vec.push(robot_un);
        deplacement(&mut robots_vec, &mut state, &mut world_size);
        assert_eq!(state[0][1], '.');
    }

    #[test]
    // Test de creatiion de robot
    fn _test_new_robot() {
        let a = new_robot(1, 'r'.to_string(), 'r', 1, 1, vec!['F', 'R', 'L']);
        let robot_test: Robots = Robots {
            id: 1,
            orientation: 'r'.to_string(),
            fleche: 'r',
            x_pos: 1,
            y_pos: 1,
            commande: vec!['F', 'R', 'L'],
        };
        assert_eq!(a, robot_test);
    }
}
