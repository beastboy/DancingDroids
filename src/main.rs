use rand::Rng;
use std::io;
mod functions;
use functions::new_robot;
use functions::Robots;
fn rando(x: usize) -> usize {
    let mut rng = rand::thread_rng();
    rng.gen_range(1, x) + 1
}
/// Print les etats des robots
fn etat(robots_vec: &Vec<Robots>) {
    let a = robots_vec.len();
    for i in 0..a {
        println!(
            "id : {}, x : {}, Y : {}",
            robots_vec[i].id, &robots_vec[i].y_pos, robots_vec[i].x_pos
        )
    }
}
fn rand_world(rand_world_size: Vec<usize>) -> Vec<Robots> {
    let mut rand_robots_vec: Vec<Robots> = Vec::new();
    let instructions: Vec<char> = vec!['F', 'R', 'L', 'H'];
    let mut x;
    let mut y;
    // creations du robot
    for i in 0..rando(7) {
        let mut rand_instructions: Vec<char> = Vec::new();
        for _i in 0..9 {
            rand_instructions.push(instructions[rando(2)]);
        }
        loop {
            x = rando(rand_world_size[0]);
            y = rando(rand_world_size[1]);
            if x >= 2 && y >= 2 {
                break;
            }
            println!("{} {}", x, y)
        }
        rand_robots_vec.push(new_robot(
            i as i32,
            "N".to_string(),
            '_',
            x,
            y,
            rand_instructions,
        ));
    }
    rand_robots_vec
}
fn main() {
    let mut input = String::new();
    println!("choisissez : 1) Normal  2) Obstacle 3) Random");
    let _i = io::stdin().read_line(&mut input);
    let input: i32 = match input.trim_end().parse() {
        Ok(num) => num,
        Err(_) => return println!("Ce n'est pas un nombre"),
    };
    let (mut world_size, mut robots_vec) = functions::normalize();
    let mut state = vec![vec!['.'; world_size[0]]; world_size[1]];
    // OBSTACLE
    if input != 3 {
        if robots_vec.len() == 0 {
            println!("Veuillez refaire le fichier robot.txt, espace incorrect")
        };
        if input == 2 {
            if world_size[0] >= 2 {
                for _i in 0..rando(world_size[0] - 1) {
                    state[rando(world_size[0] - 1)][rando(world_size[1] - 1)] = '-';
                }
            }
        }
        if world_size.len() == 2 {
            println!("Départ");
            etat(&robots_vec);
            functions::deplacement(&mut robots_vec, &mut state, &mut world_size);
            functions::affichage(&mut state, &robots_vec, &mut world_size);
            println!("Arrivée");
            etat(&robots_vec);
        } else {
            println!("Veuillez refaire le fichier robot.txt, espace incorrect");
        }
    }
    if input == 3 {
        let rand_world_size: Vec<usize> = vec![rando(7), rando(10)];
        let rand_state = vec![vec!['.'; rand_world_size[0]]; rand_world_size[1]];
        let mut rand_robots_vec: Vec<Robots> = rand_world(rand_world_size);

        println!("Départ");
        etat(&robots_vec);
        functions::deplacement(&mut rand_robots_vec, &mut state, &mut world_size);

        println!("Arrivée");
        etat(&robots_vec);
    }
  
}
